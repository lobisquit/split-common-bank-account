function parse_currency(str) {
    number = Number.parseFloat(str);

    if (Number.isNaN(number)) {
	throw new Error('Uno degli input non � un numero valido');
    }

    if (number < 0) {
	throw new Error('Uno degli input � negativo');
    }

    return number;
}

function compute(form) {
    try {
        stipendio_lobi = parse_currency(form.elements.stipendio_lobi.value);
        stipendio_soma = parse_currency(form.elements.stipendio_soma.value);

        saldo_attuale = parse_currency(form.elements.saldo_attuale.value);
        saldo_finale = parse_currency(form.elements.saldo_finale.value);

        if (saldo_finale - saldo_attuale <= 0) {
            throw new Error('L\'obbiettivo di saldo � meno del saldo attuale');
        }
        contributo = saldo_finale - saldo_attuale;

        contributo_soma = contributo / (1 + stipendio_lobi / stipendio_soma);
        contributo_lobi = contributo - contributo_soma;

        document.getElementById('outcome').removeAttribute("hidden");

        document.getElementById("contributo_soma")
            .textContent = contributo_soma.toFixed();

        document.getElementById("contributo_lobi")
            .textContent = contributo_lobi.toFixed();
    }

    catch (e) {
        alert(e);
    }

    return false;
}
